## merlin-user 11 RP1A.200720.011 V12.0.1.0.RJOMIXM release-keys
- Manufacturer: xiaomi
- Platform: mt6768
- Codename: merlin
- Brand: Redmi
- Flavor: aosp_merlin-user
- Release Version: 11
- Id: RQ3A.210805.001.A1
- Incremental: eng.clashn.20210813.104845
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/merlin/merlin:11/RP1A.200720.011/V12.0.1.0.RJOMIXM:user/release-keys
- OTA version: 
- Branch: merlin-user-11-RP1A.200720.011-V12.0.1.0.RJOMIXM-release-keys
- Repo: redmi_merlin_dump_7074


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
